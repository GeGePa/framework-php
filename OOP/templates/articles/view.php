<?php include __DIR__.'/../header.php';?> 
    <a href="/article/<?=$article->getId();?>/edit">Изменить статью</a>
    <h2><?= $article->getName()?></h2>
    <h3>Автор: <?= $article->getAuthor()->getNickname()?></h3>
    <p><?= $article->getText()?></p>
    <hr>

    <a href="/article/<?=$article->getId();?>/comments">Посмотреть комментарии к статье</a>
<?php include __DIR__.'/../footer.php';?> 

       