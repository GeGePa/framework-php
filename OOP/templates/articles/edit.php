<?php include __DIR__.'/../header.php';?> 
    <h2>Изменение статьи</h2>
    <!-- <hr> -->
    <form method="POST" class="edit-form">
      <fieldset class="edit-form__fieldset">
        <legeng></legeng>
        <label for="name">Название статьи</label><br>
        <input type="text" name="name" id="name" value="<?= $_POST['name'] ?? $article->getName() ?>" require>
        <label for="text">Текст статьи</label><br>
        <textarea class="edit-form__textarea" name="text" id="text" cols="30" rows="10" require><?= $_POST['text'] ?? $article->getText() ?></textarea>
        <button type="submit" name="edit-article">Изменить запись</button>
      </fieldset>
    </form>
<?php include __DIR__.'/../footer.php';?> 