<?php include __DIR__ . '/../../header.php'; ?>

<a href="/article/<?=$name->getId();?>">Вернуться к статье</a>

<form method="POST" class="comment-form">
  <fieldset class="edit-form__fieldset">
    <legeng></legeng>
    <label for="text">Текст комментария</label><br>
    <textarea class="edit-form__textarea" name="text" id="text" cols="30" rows="5" require><?= $_POST['text'] ?? '' ?></textarea>
    <button type="submit" name="comment-article">Оставить комментарий</button>
  </fieldset>
</form> 

<h2>Комментарии к статье "<?= $name->getName() ?>"</h2>
<hr>
<?php foreach ($comments as $comment) : ?>
  <h3 id="<?= 'comment' . $comment->getId() ?>"> #<?= $comment->getId() . ' ' . $comment->getAuthor()->getNickname() ?></h3>
  <p><?= $comment->getText() ?></p>
  <a href="/article/<?=$name->getId();?>/comments/<?=$comment->getId()?>/edit">Редактировать комментарий</a>
  <hr>
<?php endforeach; ?>


<?php include __DIR__ . '/../../footer.php'; ?>