<?php

namespace MyProject\Models\Comments;

use MyProject\Models\Users\User;
use MyProject\Models\ActiveRecordEntity;
use MyProject\Models\Articles\Article;
use MyProject\Services\Db;

class Comment extends ActiveRecordEntity
{
  protected $authorId;
  protected $articleId;
  protected $text;
  protected $createdAt;

  public function getAuthor()
  {;
    return User::getById($this->authorId);
  }
  public function getArticle()
  {
    return Article::getById($this->articleId);
  }
  public function getText()
  {
    return $this->text;
  }

  public function updateFromArray(array $fields): Comment
  {
    $this->setArticle(Article::getById($fields['articleId']));
    $this->setText($fields['text']);
    $this->setAuthor(User::getById($fields['authorId']));

    $this->save();

    return $this;
  }

  public function getNewId() {
    $db = Db::getInstance();
    $lastId = $db->query('SELECT * FROM  `'.static::getTableName().'` ORDER BY id DESC LIMIT 1', [], static::class);
    return $lastId[0]->getId();
  }

  public function setText(string $text)
  {
    $this->text = $text;
  }
  public function setAuthor(User $author)
  {
    $this->authorId = $author->id;
  }
  public function setArticle(Article $article)
  {
    $this->articleId = $article->id;
  }
  public static function getTableName(): string
  {
    return 'comments';
  }
}
