<?php

namespace MyProject\Controllers;

use MyProject\View\View;
use MyProject\Models\Articles\Article;
use MyProject\Models\Users\User;
use MyProject\Models\Comments\Comment;


class CommentComtroller
{
  private $view;
  private $db;
  public function __construct()
  {
    $this->view = new View(__DIR__ . '/../../../templates');
  }

  public function view(int $idArticle)
  {
    $article = Article::getById($idArticle);
    $comments = Comment::findAllComments($idArticle);
    $comment = Comment::getById($idArticle);

    if (!empty($_POST)) {
      if ($_POST['id'] == 0) $comment = new Comment();
      $_POST['articleId'] = $idArticle;
      $_POST['authorId'] = 2;
      $comment->updateFromArray($_POST);
      header('Location: /article/' . $idArticle . '/comments#comment' . $comment->getNewId(), true, 302);
      exit();
    }
    $this->view->renderHtml('articles/comments/view.php', ['name' => $article, 'comments' => $comments]);
  }

  public function edit(int $idArticle, int $idComment)
  {
    $article = Article::getById($idArticle);
    $comment = Comment::getById($idComment);

    if (($comment == []) || ($article == [])) {
      $this->view->renderHtml('errors/404.php', [], 404);
      return;
    }

    if (!empty($_POST)) {
      $_POST['id'] = $idComment;
      $_POST['articleId'] = $idArticle;
      $_POST['authorId'] = 2;
      $comment->updateFromArray($_POST);
      header('Location: /article/' . $idArticle . '/comments#comment' . $idComment, true, 302);
      exit();
    }

    $this->view->renderHtml('articles/comments/edit.php', ['name' => $article, 'comment' => $comment]);
  }

}
